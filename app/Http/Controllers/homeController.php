<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use App\Models\Post;

class homeController extends Controller
{
    // public function order(){
    //     $hdmn = DB::table('orders')->get();

        
    //     $sp = DB::table('products')->get();
    //     $users = DB::table('area')->join('orders', 'area.id', '=', 'orders.area_id')->get();

    //     $cthd = DB::table('order_detail')
    //         ->join('products', 'products.id', '=', 'order_detail.product_id')
    //         ->join('orders', 'orders.id', '=', 'order_detail.order_id')
    //         ->get();
    //     // $namearea = DB::table('area')
    //     //     ->select('id')
    //     //     ->join('orders', 'orders.area_id', '=', 'area.id')
    //     //     ->where('area.name', 'Miền nam')
    //     //     ->get();
    //     // dd($cthd);

    //     return view('home',compact('sp','cthd','users'));

    // }

    public function home(){

        
        
        return view('main');
    }



    function main($date)
    {


        $dt=$date;
        
        $ctmn = DB::table('provinces')->get();

        
        $prod = DB::table('products')->get();

        

        $cthd = DB::table('order_detail')
            ->join('orders', 'orders.id', '=', 'order_detail.order_id')
            ->whereDate('order_detail.created_at', '=', $date)
            ->get(); 

        $tinh = DB::table('provinces')
            ->where('area_id', '=', '3')
            ->get();         
        
        
        
        return view('main',compact('dt','ctmn','prod','cthd','tinh'));
    }

    function main_v2($date_v2)
    {


        $dt=$date_v2;

        
       
        $grouporder = DB::table('v2_orders')
            ->select('area_id','col1_province_id','col2_province_id','col3_province_id','col4_province_id','col5_province_id','col6_province_id')
            ->where('area_id','=','3')
            ->groupByRaw('area_id, col1_province_id, col2_province_id, col3_province_id, col4_province_id, col5_province_id, col6_province_id')
            ->get();    

        $grouporder2 = DB::table('v2_orders')
            ->select('area_id','col1_province_id','col2_province_id','col3_province_id','col4_province_id','col5_province_id','col6_province_id')
            ->where('area_id','=','2')
            ->groupByRaw('area_id, col1_province_id, col2_province_id, col3_province_id, col4_province_id, col5_province_id, col6_province_id')
            ->get();

       
        
        $ctmn = DB::table('provinces')->get();

        
        $prod = DB::table('products')->get();

        

        $cthd = DB::table('v2_order_detail')
            ->join('v2_orders', 'v2_orders.id', '=', 'v2_order_detail.v2_order_id')
            ->where('v2_orders.area_id', '=', '3')
            
            // ->whereDate('order_detail.created_at', '=', $date_v2)
            ->get(); 

        
        $namecthd = DB::table('v2_order_detail')
            ->select('product_id','name')
            ->join('products', 'products.id', '=', 'v2_order_detail.product_id')
            ->join('v2_orders', 'v2_orders.id', '=', 'v2_order_detail.v2_order_id')
            ->whereDate('v2_orders.created_at', '=', $date_v2)
            ->where('v2_orders.area_id', '=', '3')
            ->groupByRaw('product_id, name')
            ->get();

        $namecthdmt = DB::table('v2_order_detail')
            ->select('product_id','name')
            ->join('products', 'products.id', '=', 'v2_order_detail.product_id')
            ->join('v2_orders', 'v2_orders.id', '=', 'v2_order_detail.v2_order_id')
            ->whereDate('v2_orders.created_at', '=', $date_v2)
            ->where('v2_orders.area_id', '=', '2')
            ->groupByRaw('product_id, name')
            ->get();
      




        $cthdmt = DB::table('v2_order_detail')
            ->join('v2_orders', 'v2_orders.id', '=', 'v2_order_detail.v2_order_id')
            ->where('v2_orders.area_id', '=', '2')
            ->get();    

        $tinh = DB::table('provinces')
            ->where('area_id', '=', '3')
            ->get();         
        
        // dd($cthd);
        
        return view('main_v2',compact('dt','cthdmt','ctmn','prod','cthd','tinh','grouporder','namecthd','namecthdmt','grouporder2'));
    }

}
