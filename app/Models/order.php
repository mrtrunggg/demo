<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class order extends Model
{
    use HasFactory;
    protected $table = 'orders';
    public function orderDetails()
    {
        return $this->hasMany(orderDetail::class,'order_id','id');
    }
}
