<?php

namespace Database\Seeders;
use App\Models\order;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class hoadonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('orders')->insert(
            [
                'masp' => 'SP.G082023',
                'mien' => 'Nam',
            ],                
            
        );
        DB::table('orders')->insert(
            [
                'masp' => 'SP.G072023',
                'mien' => 'Nam',
            ],               

        );

        DB::table('orders')->insert(
            [
                'masp' => 'SP.G062023',
                'mien' => 'Nam',
            ],             

        );

        DB::table('orders')->insert(
            [
                'masp' => 'SP.G052023',
                'mien' => 'Nam',
            ],             

        );

        DB::table('orders')->insert(
                        
            [
                'masp' => 'SP.G082023',
                'mien' => 'Trung',
            ],
        );
        DB::table('orders')->insert(

            [
                'masp' => 'SP.G072023',
                'mien' => 'Trung',
            ],
        );

        DB::table('orders')->insert(

            [
                'masp' => 'SP.G062023',
                'mien' => 'Trung',
            ],
        );      

        DB::table('orders')->insert(

            [
                'masp' => 'SP.G052023',
                'mien' => 'Trung',
            ],
        );

        DB::table('orders')->insert(
                        
            [
                'masp' => 'SP.G082023',
                'mien' => 'Bắc',
            ],
        );
        DB::table('orders')->insert(

            [
                'masp' => 'SP.G072023',
                'mien' => 'Bắc',
            ],
        );

        DB::table('orders')->insert(

            [
                'masp' => 'SP.G062023',
                'mien' => 'Bắc',
            ],
        );      

        DB::table('orders')->insert(

            [
                'masp' => 'SP.G052023',
                'mien' => 'Bắc',
            ],
        );
        DB::table('orders')->insert(

            [
                'masp' => 'SP.G042023',
                'mien' => 'Bắc',
            ],
        );
        DB::table('orders')->insert(

            [
                'masp' => 'SP.G032023',
                'mien' => 'Bắc',
            ],
        );
        DB::table('orders')->insert(

            [
                'masp' => 'SP.G02023',
                'mien' => 'Bắc',
            ],
        );

        DB::table('orders')->insert(
            [
                'masp' => 'SP.G042023',
                'mien' => 'Nam',
            ],             

        );

        DB::table('orders')->insert(
            [
                'masp' => 'SP.G032023',
                'mien' => 'Nam',
            ],             

        );

    }
}
