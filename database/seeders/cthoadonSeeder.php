<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class cthoadonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('order_details')->insert(

            [
                'tinh' => 'TP.HCM',
                'sodonhang' => '123',
                'mahoadon' => '1',
            ],
            

        );


        DB::table('order_details')->insert(

            [
                'tinh' => 'Đồng Tháp',
                'sodonhang' => '123',
                'mahoadon' => '1',
            ],
        );

        DB::table('order_details')->insert(

            [
                'tinh' => 'Long An',
                'sodonhang' => '123',
                'mahoadon' => '1',
            ],
        );

        DB::table('order_details')->insert(

            [
                'tinh' => 'TP.HCM',
                'sodonhang' => '332',
                'mahoadon' => '2',
            ],
        );

        DB::table('order_details')->insert(

            [
                'tinh' => 'Đồng Tháp',
                'sodonhang' => '332',
                'mahoadon' => '2',
            ],
        );

        DB::table('order_details')->insert(

            [
                'tinh' => 'Long An',
                'sodonhang' => '332',
                'mahoadon' => '2',
            ],
        );

        DB::table('order_details')->insert(

            [
                'tinh' => 'TP.HCM',
                'sodonhang' => '333',
                'mahoadon' => '3',
            ],
        );

        DB::table('order_details')->insert(

            [
                'tinh' => 'Đồng Tháp',
                'sodonhang' => '333',
                'mahoadon' => '3',
            ],
        );

        DB::table('order_details')->insert(

            [
                'tinh' => 'Long An',
                'sodonhang' => '333',
                'mahoadon' => '3',
            ],
        );

        DB::table('order_details')->insert(

            [
                'tinh' => 'TP.HCM',
                'sodonhang' => '555',
                'mahoadon' => '4',
            ],
        );

        DB::table('order_details')->insert(

            [
                'tinh' => 'Đồng Tháp',
                'sodonhang' => '556',
                'mahoadon' => '4',
            ],
        );

        DB::table('order_details')->insert(

            [
                'tinh' => 'Long An',
                'sodonhang' => '556',
                'mahoadon' => '4',
            ],
        );

        DB::table('order_details')->insert(
            [
                'tinh' => 'TP.HCM',
                'sodonhang' => '666',
                'mahoadon' => '4',
            ],
        );

        DB::table('order_details')->insert(
            [
                'tinh' => 'Đồng Tháp',
                'sodonhang' => '667',
                'mahoadon' => '4',
            ],
        );

        DB::table('order_details')->insert(
            [
                'tinh' => 'Long An',
                'sodonhang' => '667',
                'mahoadon' => '4',
            ],
        );

        DB::table('order_details')->insert(
            [
                'tinh' => 'TP.HCM',
                'sodonhang' => '777',
                'mahoadon' => '4',
            ],
        );

        DB::table('order_details')->insert(
            [
                'tinh' => 'Đồng Tháp',
                'sodonhang' => '778',
                'mahoadon' => '4',
            ],
        );

        DB::table('order_details')->insert(
            [
                'tinh' => 'Long An',
                'sodonhang' => '778',
                'mahoadon' => '4',
            ],
        );


        DB::table('order_details')->insert(
            [
                'tinh' => 'Huế',
                'sodonhang' => '123',
                'mahoadon' => '5',
            ],
        );

        DB::table('order_details')->insert(
            [
                'tinh' => 'Đã Nẵng',
                'sodonhang' => '123',
                'mahoadon' => '5',
            ],
        );

        DB::table('order_details')->insert(
            [
                'tinh' => 'Huế',
                'sodonhang' => '332',
                'mahoadon' => '6',
            ],
        );

        DB::table('order_details')->insert(
            [
                'tinh' => 'Đã Nẵng',
                'sodonhang' => '332',
                'mahoadon' => '6',
            ],
        );

        DB::table('order_details')->insert(
            [
                'tinh' => 'Huế',
                'sodonhang' => '333',
                'mahoadon' => '7',
            ],
        );

        DB::table('order_details')->insert(
            [
                'tinh' => 'Đã Nẵng',
                'sodonhang' => '333',
                'mahoadon' => '7',
            ],
        );

        DB::table('order_details')->insert(
            [
                'tinh' => 'Huế',
                'sodonhang' => '555',
                'mahoadon' => '8',
            ],
        );

        DB::table('order_details')->insert(
            [
                'tinh' => 'Đã Nẵng',
                'sodonhang' => '556',
                'mahoadon' => '8',
            ],
        );

        DB::table('order_details')->insert(
            [
                'tinh' => 'Huế',
                'sodonhang' => '666',
                'mahoadon' => '8',
            ],
        );

        DB::table('order_details')->insert(
            [
                'tinh' => 'Đã Nẵng',
                'sodonhang' => '667',
                'mahoadon' => '8',
            ],
        );

        DB::table('order_details')->insert(
            [
                'tinh' => 'Huế',
                'sodonhang' => '777',
                'mahoadon' => '8',
            ],
        );

        DB::table('order_details')->insert(
            [
                'tinh' => 'Đã Nẵng',
                'sodonhang' => '778',
                'mahoadon' => '8',
            ],
        );

        DB::table('order_details')->insert(
            [
                'tinh' => 'Miền bắc',
                'sodonhang' => '123',
                'mahoadon' => '9',
            ],
        );
        DB::table('order_details')->insert(
            [
                'tinh' => 'Miền bắc',
                'sodonhang' => '332',
                'mahoadon' => '10',
            ],
        );
        DB::table('order_details')->insert(
            [
                'tinh' => 'Miền bắc',
                'sodonhang' => '333',
                'mahoadon' => '11',
            ],
        );
        DB::table('order_details')->insert(
            [
                'tinh' => 'Miền bắc',
                'sodonhang' => '222',
                'mahoadon' => '12',
            ],
        );

        DB::table('order_details')->insert(
            [
                'tinh' => 'Miền bắc',
                'sodonhang' => '555',
                'mahoadon' => '13',
            ],
        );

        DB::table('order_details')->insert(
            [
                'tinh' => 'Miền bắc',
                'sodonhang' => '666',
                'mahoadon' => '13',
            ],
        );

        DB::table('order_details')->insert(
            [
                'tinh' => 'Miền bắc',
                'sodonhang' => '777',
                'mahoadon' => '13',
            ],
        );

        DB::table('order_details')->insert(
            [
                'tinh' => 'Miền bắc',
                'sodonhang' => '111',
                'mahoadon' => '13',
            ],
        );
    
        DB::table('order_details')->insert(
            [
                'tinh' => 'Miền bắc',
                'sodonhang' => '-111',
                'mahoadon' => '14',
            ],
        );

        DB::table('order_details')->insert(
            [
                'tinh' => 'Miền bắc',
                'sodonhang' => '-222',
                'mahoadon' => '15',
            ],
        );

        DB::table('order_details')->insert(
            [
                'tinh' => 'TP.HCM',
                'sodonhang' => '444',
                'mahoadon' => '16',
            ],
        );

        DB::table('order_details')->insert(
            [
                'tinh' => 'Đồng Tháp',
                'sodonhang' => '444',
                'mahoadon' => '16',
            ],
        );

        DB::table('order_details')->insert(
            [
                'tinh' => 'Long An',
                'sodonhang' => '444',
                'mahoadon' => '16',
            ],
        );

        DB::table('order_details')->insert(
            [
                'tinh' => 'TP.HCM',
                'sodonhang' => '-111',
                'mahoadon' => '17',
            ],
        );

        DB::table('order_details')->insert(
            [
                'tinh' => 'Đồng Tháp',
                'sodonhang' => '-111',
                'mahoadon' => '17',
            ],
        );

        DB::table('order_details')->insert(
            [
                'tinh' => 'Long An',
                'sodonhang' => '-111',
                'mahoadon' => '17',
            ],
        );

    }
}
