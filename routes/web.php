<?php

use App\Http\Controllers\homeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
    
//     return view('welcome');
// });

// Route::get('/',[homeController::class, 'order'])->name('order');
Route::get('/',[homeController::class, 'home'])->name('home');
// Route::get('/{date}',[homeController::class, 'main'])->name('main');
Route::get('/{date_v2}',[homeController::class, 'main_v2'])->name('main_v2');

